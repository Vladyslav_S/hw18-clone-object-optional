"use strict";

function copyObject(value) {
  let result = {};
  for (let key in value) {
    if (typeof value[key] !== "object") {
      result[key] = value[key];
    } else {
      const anotherObj = value[key];
      result[key] = copyObject(anotherObj);
    }
  }
  return result;
}

const object1 = {
  userName: "Vasya",
  userAge: 20,
  userFamily: {
    mother: "Mom",
    father: "Dad",
    pets: {
      cat: "Murzik",
      dog: "Sharik",
    },
  },
};

console.log(object1);
const object2 = copyObject(object1);
console.log(object2);
